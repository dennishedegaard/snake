/*global $, setTimeout */
/*
 * Snake game implemented in javascript.
 * author: Dennis Hedegaard
 */

$(function() {
    var lblTopScore = $("#lblTopScore");
    var lblScore = $("#lblScore");
    var lblState = $("#lblState");
    var lblSpeed = $("#lblSpeed");
    var lblSnakeLength = $("#lblSnakeLength");
    var txfWidth = $("#txfWidth");
    var txfHeight = $("#txfHeight");
    var btnChangeDim = $("#btnChangeDim");
    // the number of position in both directions on the board
    var dimensions = 30;
    // contains deltas and constants for width and height
    var w = undefined, h = undefined, dw = undefined, dh = undefined;

    $.initDimensions = function() {
	// get global width and height of the element
	w = $("#c").width();
	h = $("#c").height();
	dw = Math.ceil(w / dimensions);
	dh = Math.ceil(h / dimensions);
    };
    $.initDimensions();
    // put width and height in change dimension fields.
    txfWidth.val(w);
    txfHeight.val(h);
    // handle changing dimensions on the run
    btnChangeDim.click(function() {
	var neww = parseInt(txfWidth.val());
	var newh = parseInt(txfHeight.val());
	if (neww == undefined || neww <= 0 || newh == undefined || newh <= 0) {
	    alert("Bad parameters, please use positive numbers.");
	    return false;
	}
	var c = $("#c");
	c.width(neww);
	c.height(newh);
	return false;
    });

    var c = document.getElementById("c");
    // Attempt to get the 2d context of the canvas
    if (c == undefined)
	alert("unable to get canvas element.. Try getting a better browser.");
    var ctx = c.getContext("2d");
    if (ctx == undefined)
	alert("unable to get the 2d context for the canvas, please get a better browser and try again.");

    // up = 0, left = 1, right = 2, down = 3, undef = -1
    var direction = undefined;
    // used for not suiciding before an iteration
    var cached_direction = undefined;
    // contains the current position (y, x)
    var snake = undefined;
    // true of game is over
    var paused = undefined;
    // time between moves
    var sleep = undefined;
    // scores starts at 0 and increments as items are picked up
    var score = undefined;
    // the highest score achieved this session
    var topscore = undefined;
    // an array of fruits on the board
    var fruit = undefined;
    // second chance when hitting a wall, you get an extra turn to move
    var secondchance = undefined;

    // make a new game, resetting all state and starting the loop
    $.reset = function() {
	direction = 3;
	cached_direction = direction;
	var scale = 10;
	var headx = (w / dw) / 2;
	var heady = (h / dh) / 2;
	// put the snake more or less in the middle
	snake = [ [ headx, heady ], [ headx - 1, heady ], [ headx - 2, heady ],
		  [ headx - 3, heady ], [ headx - 4, heady ],
		  [ headx - 5, heady ], [ headx - 6, heady ] ];
	// initialize state
	paused = true;
	sleep = 100;
	score = 0;
	if (topscore == undefined)
	    topscore = score;
	fruit = [ -1, -1 ];
	secondchance = true;
	// generate random fruit
	$.generateFruit();
	// manually update labels
	$.updatelabels();
	// do a manual full redraw (including borders)
	$.fulldraw();
    };

    // continues from a paused state
    $._continue = function() {
	paused = false;
	setTimeout("$.proc()", sleep);
    };

    // handle global key events
    //$(document).keypress(function(evt) {
    $(document).keydown(function(evt) {
	switch (evt.keyCode ? evt.keyCode : evt.which) {
	case 38: // up
	case 87: // w
	    // don't run into own tail
	    if (direction == 3 || cached_direction == 3)
		return;
	    direction = 0;
	    if (paused)
		$._continue();
	    break;
	case 40: // down
	case 83: // s
	    // don't run into own tail
	    if (direction == 0 || cached_direction == 0)
		return;
	    direction = 3;
	    if (paused)
		$._continue();
	    break;
	case 37: // left
	case 65: // a
	    // don't run into own tail
	    if (direction == 2 || cached_direction == 2)
		return;
	    direction = 1;
	    if (paused)
		$._continue();
	    break;
	case 39: // right
	case 68: // d
	    // don't run into own tail
	    if (direction == 1 || cached_direction == 1)
		return;
	    direction = 2;
	    if (paused)
		$._continue();
	    break;
	case 114: // r
	    $.reset();
	    break;
	case 32: // space
	    if (paused)
		$._continue();
	    else
		paused = true;
	    break;
	default:
	    // console.log("undef keypress: " + evt.which)
	    break;
	}
    });

    // updates the content of the labels on the page
    $.updatelabels = function() {
	lblTopScore.html(topscore);
	lblScore.html(score);
	lblState.html((paused || direction == -1) ? "Paused" : "Running");
	lblSpeed.html(sleep);
	lblSnakeLength.html(snake.length);
    };

    // used for performing a move from oldpos to newpos, the
    // move might now be valid. Only checks border using second chance.
    // returns 0 = invalid, 1 = valid, 2 = secondchance
    $.domove = function(oldpos, newpos) {
	if (newpos <= 0 || newpos >= dimensions - 1) {
	    if (secondchance) { // if second chance, dont move
		secondchance = false;
		return 2;
	    } else { // if used second chance, die
		paused = true;
		return 0;
	    }
	} else {
	    // reset secondchance if it�s been used
	    if (!secondchance)
		secondchance = true;
	    // move tail and let caller move head after
	    $.movetail();
	    return 1;
	}
    };

    // This function moves the tail for the snake, excluding head
    $.movetail = function() {
	for ( var i = snake.length - 1; i > 0; i--) {
	    snake[i][0] = snake[i - 1][0];
	    snake[i][1] = snake[i - 1][1];
	}
    };

    // iterate current turn, moving the snake and redrawing the board
    $.proc = function() {
	if (direction == -1)
	    return;
	// update labels
	$.updatelabels();
	// easy access head of the snake
	var head = snake[0];
	// check for pause state
	if (paused) {
	    console.log("paused");
	    ctx.fillStyle = "#000000";
	    return;
	}
	// perform move
	switch (direction) {
	case 0: // up
	    if ($.domove(head[0], head[0] - 1) == 1) // if valid move
		head[0]--;
	    break;
	case 1: // left
	    if ($.domove(head[1], head[1] - 1) == 1) // if valid move
		head[1]--;
	    break;
	case 2: // right
	    if ($.domove(head[1], head[1] + 1) == 1) // if valid move
		head[1]++;
	    break;
	case 3: // down
	    if ($.domove(head[0], head[0] + 1) == 1) // if valid move
		head[0]++;
	    break;
	}
	// eat fruit on head position, if any
	if (fruit[0] == head[0] && fruit[1] == head[1]) {
	    // generate new fruit
	    $.generateFruit();
	    // increase snake length by 1
	    snake.push([ -1, -1 ]);
	    // increase speed by ~10%
	    sleep = Math.max(15, parseInt(Math.floor(sleep * .95)));
	    // increment score
	    score++;
	    // check for new top score
	    if (score > topscore)
		topscore = score;
	}
	// check for new head in tail
	for ( var i = 1; i < snake.length; i++)
	    if (snake[i][0] == head[0] && snake[i][1] == head[1]) {
		paused = true;
		break;
	    }
	cached_direction = direction;
	// if paused now, we're dead
	if (paused) {
	    console.log("dead");
	    // reset state
	    $.reset();
	    return;
	}
	// draw board
	$.draw();
	// sleep and loop
	setTimeout("$.proc()", sleep);
    };

    // does the same a draw, including borders of the board
    $.fulldraw = function() {
	// draw border
	ctx.fillStyle = "#000000";
	ctx.fillRect(0, 0, dw, h);
	ctx.fillRect(0, 0, w, dh);
	ctx.fillRect(0, h - dh, w, dh);
	ctx.fillRect(w - dw, 0, dw, h);
	// draw the rest
	$.draw();
    };

    // draw the board
    $.draw = function() {
	// clear board
	ctx.fillStyle = "#33dd55";
	ctx.fillRect(dw, dh, w - (dw * 2), h - (dh * 2));
	// draw "snake"
	for ( var i = snake.length - 1; i >= 0; i--) {
	    if (snake[i][0] == -1 || snake[i][1] == -1)
		continue;
	    if (i == 0)
		ctx.fillStyle = "#6666ff"; // head color
	    else
		ctx.fillStyle = "#000099"; // tail color
	    var _x = snake[i][1] * dw;
	    var _y = snake[i][0] * dh;
	    ctx.fillRect(_x, _y, dw, dh);
	}
	// draw fruits
	ctx.fillStyle = "#ff0000"; // fruit color
	var x = fruit[1] * dw;
	var y = fruit[0] * dh;
	ctx.fillRect(x, y, dw, dh);
    };

    // this function generates a random piece of "fruit" on the table.
    // generate a new fruit coordinate inside the board, not inside the snake
    $.generateFruit = function() {
	// generate random pair
	var tmp = [ Math.floor(Math.random() * (dimensions - 2)) + 1,
		    Math.floor(Math.random() * (dimensions - 2)) + 1 ];
	// check for snake intercept
	for ( var i in snake)
	    if (snake[i][0] == tmp[0] && snake[i][1] == tmp[1]) {
		$.generateFruit();
		return;
	    }
	fruit = tmp;
    };

    // intiate and start the main loop
    $.reset();
});
